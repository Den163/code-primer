------------------------------------------------
-- Finite State Machine for Lua/Corona v 1.0

-- A prototype for all transitions in the game

-- Do want you want with it
-- DWYW licence ©
------------------------------------------------

local transition = {}

transition.name = nil
transition.targetState = nil
transition.needTransition = false
transition.enabled = false

transition.onEnable = nil
transition.onDisable = nil
transition.onUpdate = nil

-- Transition class constructor
function transition:new(name, targetState)
  local trans = {} -- a transition object that will return
  self.__index = self
  setmetatable(trans, self)
  
  if targetState == nil then
    error("Target state for transition is null, check TransitionClass:new(name, targetState) arguments")
  end
  
  if name == nil then
    error("Transition name is null, check TransitionClass:new(name, targetState) arguments")
  end
  
  trans.targetState = targetState
  trans.name = name
  
  return trans
end

-- Transition class destructor
function transition:destruct()
  self:disable()
end

function transition:enable()
--  print(self.name.." enabled")
  self.enabled = true
  if self.onEnable then
    self.onEnable()
  end
  
  if self.onUpdate then
    Runtime:addEventListener("enterFrame", self.onUpdate)
  end
end

function transition:disable()
--  print(self.name.." disabled")
  self.enabled = false
  self.needTransition = false
  if self.onDisable then 
    self.onDisable()
  end
  
  if self.onUpdate then
    Runtime:removeEventListener("enterFrame", self.onUpdate)
  end
end

return transition