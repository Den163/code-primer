local FSM = {}

FSM.currentState = nil
FSM.beginState = nil

-- FSM Callbacks
FSM.onUpdate = nil
--

function FSM:transit(nextState)
  if self.currentState ~= nil then
--    print("Exit from previous "..self.currentState.name.." state")
    self.currentState:exit()
  end

  self.currentState = nextState

  if self.currentState ~= nil then
--    print("Enter to next "..self.currentState.name.." state")
    self.currentState:enter()
  end
end

function FSM:reset()
  self:transit(self.beginState)
end

-- Finite State Machine destructor
function FSM:close()
  if self.currentState ~= nil then
    self:graphBypassClose(self.beginState)
    Runtime:removeEventListener("enterFrame", self.onUpdate)
    self.currentState = nil
    self.states = {}
    print("State machine has been closed")
  end
end

function FSM:resume()
  if self.currentState ~= nil then
    self.currentState:resume()
  end
end

function FSM:suspend()
  if self.currentState ~= nil then
    self.currentState:suspend()
  end
end

-- Finite State Machine constructor
function FSM:new(beginState)
  local stateMachine = {} -- a State Machine Object that will return
  self.__index = self
  setmetatable(stateMachine, self)

  print("Initializing state machine...")

  if beginState == nil then
    error("Begin state for Finite State Machine is nill, check FSMClass:new(beginState) arguments")
  end

  stateMachine.currentState = beginState
  stateMachine.beginState = beginState
  stateMachine.states = {}
  beginState:enter()

  stateMachine.onUpdate = function(event)
      if stateMachine.currentState == nil then
        return
      end

      if stateMachine.currentState.onUpdate then
        stateMachine.currentState:onUpdate(event)
      end

      -- It can be nilled during update and we need to check it again
      if stateMachine.currentState == nil then
        return
      end

      local nextState = stateMachine.currentState:getNext()
      if nextState ~= nil then
        stateMachine:transit(nextState)
      end
  end

  -- checking for states every frame
  Runtime:addEventListener("enterFrame", stateMachine.onUpdate)

  return stateMachine
end

return FSM