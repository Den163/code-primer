-------------------------------------------
-- Finite State Machine for Lua/Corona v 1.0

-- A prototype for all states in the game

-- Do want you want with it
-- DWYW licence ©
-------------------------------------------
local TransitionClass = require "src.FSM.TransitionClass"

local state = {}

state.name = nil
state.enabled = false

-- state callbacks
state.onUpdate = nil
state.onEnter = nil
state.onExit = nil

state.suspendedUpdate = nil

-- State class constructor
function state:new(name)
  local st = {} -- a state object that will return
  self.__index = self
  setmetatable(st, self)

  st.transitions = {}

  if name == nil then
    error("State name is error, check StateClass:new(name) arguments")
  end

  st.name = name

  return st
end

function state:addTransition(name, targetState, predicate)
  self.transitions[#self.transitions + 1] = TransitionClass:new(name, targetState)
  self.transitions[#self.transitions].predicate = predicate
end

-- Argument transitions is like { {name, targetState}, {name, targetState} }
function state:addTransitions(transitions)
  for i = 1, #transitions do
    self.transitions[#self.transitions + 1] = TransitionClass:new(transitions[i].name, transitions[i].targetState)
  end
end

function state:readyToTransit(targetState)
  print( string.format("State '%s' has %i transition(s) ", self.name,  #self.transitions) )
  for i = 1, #self.transitions do
    if targetState == self.transitions[i].targetState then
      self.transitions[i].needTransition = true
      print( string.format("Transit from '%s' state to '%s' state", self.name, self.transitions[i].targetState.name) )
      return
    end
  end

  print("There is no transition from "..self.name.." to "..targetState.name)
end

-- State class destructor
function state:destruct()
  for i = #self.transitions, 1, -1 do
    self.transitions[i]:destruct()
    self.transitions[i] = nil
  end

  self:exit()
end

-- Get next state if it needs transition to
function state:getNext()
  for i = 1, #self.transitions do
    if self.transitions[i].predicate and self.transitions[i]:predicate() then
      return self.transitions[i].targetState
    end
  end
end

-- Exit from the state
function state:exit()
  if self.enabled == true then
    self.enabled = false

    if self.onUpdate then
      --Runtime:removeEventListener("enterFrame", self._update)
    end

    -- print("Exit from "..self.name.." state")
    for i = 1, #self.transitions do
      self.transitions[i]:disable()
    end

    if self.onExit then
      self:onExit()
    end
  end
end

-- Enter to the state
function state:enter()
  if self.enabled == false then
    self.enabled = true


    if self.onEnter then
      self:onEnter()
    end

    --[[
    if self.onUpdate then
      self._update = function() self:onUpdate() end -- Create a function with saving "self" context
     -- Runtime:addEventListener("enterFrame", self._update)
    end
    ]]

--  print("Enter to "..self.name.." state")
    for i = 1, #self.transitions do
      self.transitions[i]:enable()
    end
  end
end

function state:suspend()
  if self.onUpdate then
    self.suspendedUpdate = self.onUpdate
    self.onUpdate = nil
   -- Runtime:removeEventListener("enterFrame", self._update)
  end
end

function state:resume()
  if self.suspendedUpdate then
    self.onUpdate = self.suspendedUpdate
  --  Runtime:addEventListener("enterFrame", self._update)
    self.suspendedUpdate = nil
  end
end

return state