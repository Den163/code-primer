local BaseBuilder = require "src.SceneBuilder.SceneBuilder"

local imageUtils = require "src.utils.imageUtils"
local gameConfig = require "src.config.GameConfig"
local saveLoadJSON = require "src.utils.saveLoadJSON"
local composer = require "composer"

local HowToPlayBuilder = BaseBuilder:extend()

function HowToPlayBuilder:buildGroups()
    local this = self
    local sceneGroup = this.sceneView.sceneGroup

    this.backgroundGroup = display.newGroup()
    this.uiGroup = display.newGroup()

    sceneGroup:insert(this.backgroundGroup)
    sceneGroup:insert(this.uiGroup)
end

function HowToPlayBuilder:buildBackgrounds()
    local this = self
    local backgroundGroup = this.backgroundGroup

    this.background = display.newImageRect(PICTURES.."bg_cover.png", 320, 480)
    this.background:translate(display.contentCenterX, display.contentCenterY)
    imageUtils.resizeRelativeTo( this.background, display.actualContentWidth, display.actualContentHeight )

    backgroundGroup:insert(this.background)
end

function HowToPlayBuilder:buildUI()
    local this = self
    local uiGroup = this.uiGroup

    this.backButton = GetSprite("btn_back")
    this.backButton:translate(SCREEN_LEFT + 8, SCREEN_BOTTOM + 1.5)
    this.backButton.anchorX, this.backButton.anchorY = 0, 1
    this.backButton:scale(0.85, 0.85)

    this.backButton:addEventListener("tap", function() this:onBackButtonClick() end)

    this.nextButton = GetSprite("btn_next")
    this.nextButton:translate(SCREEN_RIGHT - 7, SCREEN_BOTTOM + 1.5)
    this.nextButton.anchorX, this.nextButton.anchorY = 1, 1
    this.nextButton:scale(0.85, 0.85)

    this.nextButton:addEventListener("tap", function() this:onNextButtonClick() end)

    this:buildScreens();

    uiGroup:insert(this.backButton)
    uiGroup:insert(this.nextButton)
end

function HowToPlayBuilder:buildScreens()
    local this = self
    local backgroundGroup = this.backgroundGroup
    local uiGroup = this.uiGroup

    this.currentScreen = 1;

    this.firstScreen = display.newImageRect(PICTURES.."tutorial_1.png", 320, 310);
    this.firstScreen:translate(display.contentCenterX, display.contentCenterY + 12)

    this.secondScreen = display.newImageRect(PICTURES.."tutorial_2.png", 187, 340)
    this.secondScreen:translate(display.contentCenterX, display.contentCenterY + 12)
    this.secondScreen.isVisible = false;

    this.thirdScreen = display.newImageRect(PICTURES.."tutorial_3.png", 291, 322)
    this.thirdScreen:translate(display.contentCenterX, display.contentCenterY + 8)
    this.thirdScreen.isVisible = false;

    this.screens = { this.firstScreen, this.secondScreen, this.thirdScreen }

    uiGroup:insert(this.firstScreen)
    uiGroup:insert(this.secondScreen)
    uiGroup:insert(this.thirdScreen)


    this.title = display.newBitmapText("HOW TO PLAY", 0, 0, "f1", 82)
    this.title.anchorY = 0
    this.title:translate(display.contentCenterX, (this.firstScreen.y - this.firstScreen.contentHeight / 2)  * 0.5);
    imageUtils.resizeTo(this.title, display.actualContentWidth, this.title.contentHeight)

    backgroundGroup:insert(this.title)
end

function HowToPlayBuilder:onBackButtonClick()
    local this = self

    if (this.currentScreen == 1) then
        local previous = composer.getSceneName("previous")
        composer.gotoScene(previous);
        return;
    end

    this.nextButton.isVisible = true;

    this.screens[this.currentScreen].isVisible = false
    this.currentScreen = this.currentScreen - 1
    this.screens[this.currentScreen].isVisible = true
end

function HowToPlayBuilder:onNextButtonClick()
    local this = self
    local previous = composer.getSceneName("previous")

    if (previous == "MainMenu" and this.currentScreen == 3) then
        saveLoadJSON.saveJSON("jellyDropFirstEntry", { false });
        composer.gotoScene("Game");
        return;
    end

    this.screens[this.currentScreen].isVisible = false;
    this.currentScreen = this.currentScreen + 1

    this.screens[this.currentScreen].isVisible = true;

    if (this.currentScreen == 3 and previous ~= "MainMenu") then
        this.nextButton.isVisible = false;
    end
end

return HowToPlayBuilder