local SceneBuilder = {}

function SceneBuilder:buildVariables()
end

function SceneBuilder:buildGroups()
end

function SceneBuilder:buildBackgrounds()
end

function SceneBuilder:buildForegrounds()
end

function SceneBuilder:buildUI()
end

function SceneBuilder:build()
  self.sceneView = {}
  self.sceneView.sceneGroup = display.newGroup()

  self:buildVariables()
  self:buildGroups()
  self:buildBackgrounds()
  self:buildForegrounds()
  self:buildUI()

  return self.sceneView
end

function SceneBuilder:extend()
  local extendedBuilder = {}

  self.__index = self
  setmetatable(extendedBuilder, self)

  return extendedBuilder
end

function SceneBuilder:new()
  local newSceneBuilder = {}

  self.__index = self
  setmetatable(newSceneBuilder, self)

  return newSceneBuilder
end

return SceneBuilder