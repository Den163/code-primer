local composer = require "composer"
local Scene = {}
local GarbageManager = Managers.GarbageManager

-- Virtual
function Scene:onCreate(event)
end

-- Virtual
function Scene:onHide(event)
end

function Scene:create(event)
  self.sceneView = self.sceneBuilder:build()
  self.view:insert(self.sceneView.sceneGroup)
  self:onCreate(event)
end

function Scene:hide(event)
  self:onHide(event)
  if event.phase == "will" then
    local currentScene = composer.getSceneName("current")
    composer.removeScene(currentScene, true)
  elseif event.phase == "did" then
    GarbageManager:clean()
    print("Exit from scene")
  end
end

function Scene:super()
  return Scene
end

function Scene:extend()
  local extendedObject = {}

  self.__index = self
  setmetatable(extendedObject, self)

  return extendedObject
end

function Scene:new(name, sceneBuilder)
  self.__index = self

  local proxy = composer.newScene(name)

  local newScene = {}
  setmetatable(newScene, {__index = function (table, key) return self[key] or proxy[key] end})

  newScene.name = name
  newScene.sceneBuilder = sceneBuilder

  proxy:addEventListener("create", newScene)
  proxy:addEventListener("hide", newScene)

  return newScene
end

return Scene