---------------------------------------------------------------------
--         *** INTERFACE FOR ALL COMPONENTS (OR COMPOSITES) ***        --
---------------------------------------------------------------------

local BaseComponent = {}

function BaseComponent:setPosition(x, y)
  error("You do not override setPosition(group) method in subclass")
end

function BaseComponent:setScale(xScale, yScale)
  error("You do not override setScale(group) method in subclass")
end

function BaseComponent:setGroup(group)
  error("You do not override setScale(group) method in subclass")
end

function BaseComponent:setColor(color, exposure)
  error("You do not override setColor(color, exposure) method in subclass")
end

function BaseComponent:getName()
  return self.metaData.name
end

function BaseComponent:getPosition()
  error("Not implemented getPosition() method")
end

function BaseComponent:getScale()
  error("Not implemented getScale() method")
end

function BaseComponent:getZ()
  return self.metaData.z
end

function BaseComponent:getParent()
  return self.parent
end

function BaseComponent:setName(name)
  self.metaData.name = name
end

function BaseComponent:setParent(parent)
  self.parent = parent
end

function BaseComponent:hide()
  error("You do not override hide() method in subclass")
end

function BaseComponent:serialize(characterDataTree)
  error("You do not override jsonSerialize() method in subclass")
end

function BaseComponent:deserialize(dataNode)
  error("You do not override jsonSerialize() method in subclass")
end

function BaseComponent:destroy()
  error("You do not override destroy() method in subclass")
end

function BaseComponent:removeRender()
    error("Invalid operation removeRender()")
end

function BaseComponent:extend()
  local extendedObject = {}

  self.__index = self
  setmetatable(extendedObject, self)

  return extendedObject
end

return BaseComponent