local BaseComponent = require "src.CharacterComposite.BaseComponent"
local SpriteComponent = BaseComponent:extend()

local colorDataBase = require "src.data.colorDataBase"
local hextoRGB = require "src.utils.hexToRGB"

if NaN_COLOR == nil then error("Please specify global 'NaN_COLOR' constant (it equals - 1)") end

function SpriteComponent:new(spriteSheet, sprite)
  local componentObject = {}
  local this = componentObject

  self.__index = self
  setmetatable(componentObject, self)

  componentObject.metaData = {}

  this.view = GetSprite(spriteSheet, sprite)
  this.group = display.newGroup()
  this.group:insert(this.view)

  -- MetaData for save object
  local metaData = this.metaData
  metaData.spriteSheet = spriteSheet
  metaData.sprite = sprite

  return componentObject
end

function SpriteComponent:getGroup()
  return self.group
end

function SpriteComponent:getColor()
  return self.metaData.color
end

function SpriteComponent:getExposure()
  return self.metaData.exposure
end

function SpriteComponent:getPosition()
  return {
    x = self.view.x,
    y = self.view.y
  }
end

function SpriteComponent:getSpriteName()
  return self.metaData.sprite
end

function SpriteComponent:getSpriteSheetName()
  return self.metaData.spriteSheet
end

function SpriteComponent:getScale()
  return {
    xScale = self.view.xScale,
    yScale = self.view.yScale
  }
end

function SpriteComponent:getWidth()
  return self.view.contentWidth
end

function SpriteComponent:getHeight()
  return self.view.contentHeight
end

function SpriteComponent:getName()
  return self.metaData.name
end

function SpriteComponent:setGroup(group)
  local this = self

  group:insert(this.group)
end

function SpriteComponent:setPosition(x, y)
  local this = self

  this.view.x = x
  this.view.y = y
end

-- Set view's color by specified in metaData
function SpriteComponent:setColor(hexColor, exposure)
  local this = self

  if hexColor == nil then return end

  this.metaData.color = hexColor

  if hexColor == NaN_COLOR then
    hexColor = 0xFFFFFF
    exposure = 0
  end

  if (exposure) then
    this.metaData.exposure = exposure
    this.view.fill.effect = "filter.exposure"
    this.view.fill.effect.exposure = exposure
  end

  -- Translate color from hex format
  local r, g, b = hextoRGB.hexToRgb(hexColor)

  -- #TEST
  if this.view == nil then
    local errorMessage = ""
    for key, value in pairs(this.metaData) do
      errorMessage = errorMessage..( string.format( "%s\n%s - %s", errorMessage, key, value ) )
    end

    error(errorMessage)
  end
  -- #END_TEST

  assert( this.view.setFillColor,
    string.format( "The view of sprite component /'%s/' has been destroyed", self.metaData.name or "!!uknown!!" )
  )

  this.view:setFillColor(r, g, b, 1)
end

function SpriteComponent:setScale(xScale, yScale)
  local this = self

  this.view.xScale = xScale
  this.view.yScale = yScale
end

-- Warning!!! This method do not affect on layer hierarchy
-- It's just save z coordinate of component
-- To change layer herarchy use Composite:setZ()
function SpriteComponent:setZ(z)
  self.metaData.z = z
end

function SpriteComponent:hide()
  local this = self

  this.view.isVisible = false
end

function SpriteComponent:show()
  local this = self

  this.view.isVisible = true
end

function SpriteComponent:serialize(characterDataTree)
  local this = self
  local metaData = this.metaData
  local parts = this.parts

  characterDataTree[metaData.name] = {}

  local treeNode = characterDataTree[metaData.name]

  treeNode.name = metaData.name
  treeNode.spriteSheet = metaData.spriteSheet
  treeNode.sprite = metaData.sprite
  treeNode.color = metaData.color
  treeNode.exposure = metaData.exposure
  treeNode.xScale = this.view.xScale
  treeNode.yScale = this.view.yScale
  treeNode.x = this.view.x
  treeNode.y = this.view.y
  treeNode.z = this.metaData.z

  return characterDataTree
end

function SpriteComponent:deserialize(dataNode)
  local newComponent = SpriteComponent:new(dataNode.spriteSheet, dataNode.sprite)
  newComponent:setPosition(dataNode.x or 0, dataNode.y or 0)
  newComponent:setScale(dataNode.xScale or 1, dataNode.yScale or 1)
  newComponent:setColor(dataNode.color, dataNode.exposure)

  return newComponent
end

function SpriteComponent:destroy()
  local this = self

  display.remove(this.view)
  this.view = nil
end

return SpriteComponent