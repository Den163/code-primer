local BaseComponent = require "src.CharacterComposite.BaseComponent"
local ImageComponent = require "src.CharacterComposite.ImageComponent"
local SpriteComponent = require "src.CharacterComposite.SpriteComponent"
local EmptyComponent = require "src.CharacterComposite.EmptyComponent"
local Composite = BaseComponent:extend()

function Composite:new(name)
  local compositeObject = {}

  self.__index = self
  setmetatable(compositeObject, self)
  compositeObject.metaData = {}


  compositeObject:setName(name)
  compositeObject.group = display.newGroup()
  compositeObject.metaData.z = 1

  compositeObject.groups = {}
  compositeObject.parts = {}

  return compositeObject
end

local swapParts

-- Add or replace part of character
function Composite:addPart(name, part)
  local this = self
  local parts = this.parts
  local newGroup
  local groupIndex

  if part == nil then error("Choose correct part as the function argument") end

  if parts[name] then
    -- Remember group of existing part (to save layout)
    groupIndex = table.indexOf(this.groups, parts[name].group.parent)
    newGroup = this.groups[groupIndex]

    swapParts( part, parts[name] )
  else
    newGroup = display.newGroup()
    this.groups[#this.groups + 1] = newGroup
    groupIndex = #this.groups
    this.group:insert(newGroup)
  end

  parts[name] = part
  part:setName(name)
  part:setParent(this)

  this:setPartGroup(name, groupIndex)
  return part
end

function swapParts( newPart, oldPart)
  local color = oldPart:getColor()
  local position = oldPart:getPosition()
  local scale = oldPart:getScale()

  if color then
    newPart:setColor(color)
  end

  newPart:setPosition( position.x, position.y )
  newPart:setScale( scale.xScale or 1, scale.yScale or 1 )

  oldPart:destroy()
end

-- Returns component object by the name if it's exist or nil if not
function Composite:getPart(name)
  local this = self
  local parts = this.parts

  if this.metaData.name == name then
    return this
  end

  if parts[name] then
    return parts[name]
  else
    return this:_find(name)
  end
end

function Composite:removePart(name)
  local this = self
  local parts = this.parts

  parts[name]:destroy()
end

function Composite:getScale(xScale, yScale)
  local this = self
  return {
    x = this.group.xScale,
    y = this.group.yScale
  }
end

function Composite:getGroup()
  return self.group
end

function Composite:setGroup(group)
  local this = self
  group:insert(this.group)
  --[[
  local oldGroup = this.group
  this.group = group
  display.remove(oldGroup)


  for i = 1, #this.groups do
    this.group:insert(this.groups[i])
  end
  ]]
end

function Composite:setPartGroup(name, index)
  local this = self
  local parts = this.parts

  parts[name].metaData.z = index
  parts[name]:setGroup(this.groups[index])
end

function Composite:getPosition()
  return {
    x = self.group.x,
    y = self.group.y
  }
end

function Composite:setPosition(x, y)
  local this = self

  if x ~= nil then
    this.group.x = x
  end

  if y ~= nil then
    this.group.y = y
  end
end

function Composite:setColor()
  local this = self

  for _, part in pairs(this.parts) do
    part:setColor()
  end
end

function Composite:getScale()
  return {
    xScale = self.group.xScale,
    yScale = self.group.yScale
  }
end

function Composite:setScale(xScale, yScale)
  local this = self

  this.group.xScale = xScale
  this.group.yScale = yScale
end

function Composite:setZ(partName, z)
  local this = self

  this.groups[z]:insert(this.parts[partName].group)
  this.parts[partName].metaData.z = z
end

function Composite:swapGroups(partA, partB)
  local this = self
  local groupIndexA = table.indexOf(this.groups, this.parts[partA].group)
  local groupIndexB = table.indexOf(this.groups, this.parts[partB].group)

  this:setPartGroup(partA, groupIndexB)
  this:setPartGroup(partB, groupIndexA)
end

function Composite:serialize(characterDataTree)
  local this = self
  local metaData = this.metaData
  local parts = this.parts

  characterDataTree[metaData.name] = {}

  local treeNode = characterDataTree[metaData.name]

  treeNode.name = metaData.name
  treeNode.xScale = this.group.xScale
  treeNode.yScale = this.group.yScale
  treeNode.x = this.group.x
  treeNode.y = this.group.y
  treeNode.z = this.metaData.z
  treeNode.color = this.metaData.color

  treeNode.parts = {}

  for key, _ in pairs(parts) do
    parts[key]:serialize(treeNode.parts)
  end

  return characterDataTree
end

local function CompositeCompare(elementA, elementB)
  if     elementA.z == nil then return true
  elseif elementB.z == nil then return false
  else                          return elementA.z < elementB.z
  end
end

-- dataNode is a characterDataTree node
function Composite:deserialize(dataNode, group)
  local this = self
  local name

  for key, _ in pairs(dataNode) do
    name = key
    break
  end

  local newComposite = Composite:new(name)

  local temporarySortArray = {}
  local i = 1

  if dataNode.parts == nil then error("There are no parts hash table") end

  for key, value in pairs(dataNode.parts) do
      temporarySortArray[i] = value
      temporarySortArray[i].name = key
      i = i + 1
  end

  table.sort(temporarySortArray, CompositeCompare)

  for i = 1, #temporarySortArray do
    local part
    local dataNode = temporarySortArray[i]

    if dataNode.sprite and dataNode.sprite ~= "" then
      part = SpriteComponent:deserialize(dataNode)
    elseif dataNode.image and dataNode.image ~= "" then
      part = ImageComponent:deserialize(dataNode)
    elseif dataNode.parts then
      part = Composite:deserialize(dataNode)
    else
      part = EmptyComponent:deserialize(dataNode)
    end

    newComposite:addPart(dataNode.name, part)
  end

  newComposite:setPosition(dataNode.x or 0, dataNode.y or 0)
  newComposite:setScale(dataNode.xScale or 1, dataNode.yScale or 1)
  newComposite:setColor()

  return newComposite
end

function Composite:destroy()
  local this = self
  local parts = this.parts

  for key, _ in pairs(parts) do
    parts[key]:destroy()
  end

  for i = 1, #this.groups do
    display.remove(this.groups[i])
    this.groups[i] = nil
  end
end

function Composite:removeRender()
  local this = self

  for key, value in pairs(this.parts) do
    self:addPart(key, EmptyComponent:new())
  end
end

-- Private members

-- Search in tree
function Composite:_find(name)
  local this = self
  local parts = this.parts

  for key, _ in pairs(parts) do
    if parts[key].getPart then
      local obj = parts[key]:getPart(name)
      if obj then return obj end
    end
  end
end

function Composite:sort()
  local this = self
  local parts = this.parts

  for key, value in pairs(parts) do
    this:setPartGroup(key, value.metaData.z)
  end
end

return Composite