local SpriteComponent = require "src.CharacterComposite.SpriteComponent"
local ImageComponent = SpriteComponent:extend()

function ImageComponent:newRect(image, width, height, baseDirectory)
    local componentObject = {}
    local this = componentObject

    self.__index = self
    setmetatable(componentObject, self)

    componentObject.metaData = {}

    if baseDirectory then
        this.view = display.newImageRect(image, baseDirectory, width, height)
        this.metaData.DocumentsDirectory = true
    else
        this.view = display.newImageRect(image, width, height)
    end

    if this.view == nil then
        error("The specified image \'"..image.."\' doesn't exist")
    end

    this.group = display.newGroup()
    this.group:insert(this.view)

    -- MetaData for save object
    local metaData = this.metaData
    metaData.image = image

    return componentObject
end

function ImageComponent:new(image, baseDirectory)
    local componentObject = {}
    local this = componentObject

    self.__index = self
    setmetatable(componentObject, self)

    componentObject.metaData = {}

    if baseDirectory then
        this.view = display.newImage(image, baseDirectory)
        this.metaData.DocumentsDirectory = true
    else
        this.view = display.newImage(image)
    end

    if this.view == nil then error("The specified image \'"..image.."\' doesn't exist") end

    this.group = display.newGroup()
    this.group:insert(this.view)

    -- MetaData for save object
    local metaData = this.metaData
    metaData.image = image

    return componentObject
end

function ImageComponent:serialize(characterDataTree)
    local this = self
    local metaData = this.metaData
    local parts = this.parts

    characterDataTree[metaData.name] = {}

    local treeNode = characterDataTree[metaData.name]

    treeNode.name = metaData.name
    treeNode.image = metaData.image
    treeNode.color = metaData.color
    treeNode.exposure = metaData.exposure
    treeNode.width = this.view.width
    treeNode.height = this.view.height
    treeNode.xScale = this.view.xScale
    treeNode.yScale = this.view.yScale
    treeNode.x = this.view.x
    treeNode.y = this.view.y
    treeNode.z = this.metaData.z
    treeNode.DocumentsDirectory = this.metaData.DocumentsDirectory

    return characterDataTree
end

function ImageComponent:deserialize(dataNode)
    local newComponent
    local directory = (dataNode.DocumentsDirectory) and system.DocumentsDirectory or nil

    if dataNode.width or dataNode.height then
        newComponent = ImageComponent:newRect(dataNode.image, dataNode.width, dataNode.height, directory)
    else
        newComponent = ImageComponent:new(dataNode.image, directory)
    end

    newComponent:setPosition(dataNode.x or 0, dataNode.y or 0)
    newComponent:setScale(dataNode.xScale or 1, dataNode.yScale or 1)
    newComponent:setColor(dataNode.color)

    return newComponent
end

function ImageComponent:getSpriteName()
    return self.metaData.image
end

return ImageComponent