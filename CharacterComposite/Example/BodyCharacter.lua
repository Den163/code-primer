local BaseCharacter = require "src.CharacterComposite.CharacterClass"
local CharacterFactory = require "src.CharacterComposite.CharacterFactory"
local EmptyComponent = require "src.CharacterComposite.EmptyComponent"

local bodyDataBase = require "src.data.bodyDataBase"
local clothesDataBase = require "src.data.clothesDataBase"
local bodyPartsPositionConfig = require "src.config.bodyPartsPositionConfig"

local saveLoadJSON = require "src.utils.saveLoadJSON"

local BodyCharacter = BaseCharacter:extend()

-- Numbers of shouldersSprites accordingly to body pose (bodyIndex)
BodyCharacter.handsPoses = {
    left = { 1, 1, 2, 2, 2, 1, 3, 3 },
    right = { 1, 2, 2, 1, 3, 3, 3, 1 }
}

function BodyCharacter:new()
    local super = getmetatable(self)

    local newObject = super.new(self)

    newObject.dataTree = Managers.DataManager:getBodyTreeData()
    newObject.metaData = Managers.DataManager:getCharacterConfig()
    newObject.metaData.upWearType = newObject.metaData.upWearType or Enums.UpWearTypes.Up

    return newObject
end

function BodyCharacter:show()
    local this = self

    this:removeView()

    this.view = CharacterFactory:makeFull()
    self:setBody(1)
    this.group:insert(this.view.group)
end

function BodyCharacter:removeAllClothes()
    self:setBag(NULL)
    self:setDown(NULL)
    self:setDress(NULL)
    self:setGlasses(NULL)
    self:setJewels(NULL)
    self:setShoes(NULL)
    self:setSocks(NULL)
    self:setUp(NULL)
end

function BodyCharacter:swapLayers(firstPartName, secondPartName)
    local wearComposite = self:getPart("Wear_Composite")
    local firstPart = self:getPart(firstPartName)
    local secondPart = self:getPart(secondPartName)

    local firstZ = firstPart:getZ()
    local secondZ = secondPart:getZ()

    wearComposite:setZ(firstPartName, secondZ)
    wearComposite:setZ(secondPartName, firstZ)
end

function BodyCharacter:serialize(fileName)
    assert(self.view.group, "!!!WARNING!!! There is no character composite. Cannot serialize");

    local this = self
    local characterData = {}
    local bodyTree = self:getPart("Body_Tree")

    bodyTree:serialize(characterData)

    saveLoadJSON.saveJSON(fileName, characterData)
end

function BodyCharacter:updateByPose(bodyIndex)
    self:updateUpByPose(bodyIndex)
    self:updateJewelsByPose(bodyIndex)
    self:updateBagByPose(bodyIndex)
end

function BodyCharacter:updateUpByPose(bodyIndex)
    bodyIndex = bodyIndex or self:getBodyIndex()
    local upWearType = self:getUpWearType()
    local upWearTable = (upWearType == Enums.UpWearTypes.Up) and clothesDataBase.Up or clothesDataBase.Dress
    local upPosition = (upWearType == Enums.UpWearTypes.Up) and bodyPartsPositionConfig.Up or bodyPartsPositionConfig.Dress
    local wearIndex = self:getUpWearIndex()
    local shouldersSprites = upWearTable[wearIndex]
    local wearPrefix = (upWearType == Enums.UpWearTypes.Dress) and "Dress_" or ""
    local upWearSpriteSheet = (upWearType == Enums.UpWearTypes.Dress) and "dress{n}" or "up{n}"
    local shouldersTable = self.handsPoses

    local leftShoulerIndex = shouldersTable.left[bodyIndex]
    local rightShoulderIndex = shouldersTable.right[bodyIndex]

    local leftShoulderSprite = shouldersSprites.left[leftShoulerIndex]
    local rightShoulderSprite = shouldersSprites.right[rightShoulderIndex]

    local leftShoulder = self:changePart( wearPrefix.."Left_Shoulder", upWearSpriteSheet, leftShoulderSprite )
    local rightShoulder = self:changePart( wearPrefix.."Right_Shoulder", upWearSpriteSheet, rightShoulderSprite )

    leftShoulder:setPosition(upPosition.x, upPosition.y)
    rightShoulder:setPosition(upPosition.x, upPosition.y)
end

function BodyCharacter:updateJewelsByPose(bodyIndex, jewelsIndex)
    bodyIndex = bodyIndex or self:getBodyIndex()
    jewelsIndex = jewelsIndex or self:getJewelsIndex()

    -- It means there is no jewels on the character
    if jewelsIndex == nil then return end

    self:updateNecklaceByPose(bodyIndex, jewelsIndex)
    self:updateBraceletByPose(bodyIndex, jewelsIndex)
end

function BodyCharacter:updateNecklaceByPose(bodyIndex, jewelsIndex)
    bodyIndex = bodyIndex or self:getBodyIndex()
    jewelsIndex = jewelsIndex or self:getJewelsIndex()

    local necklaceIndex = (bodyIndex == 8 or bodyIndex == 7) and 2 or 1
    local neckLace = self:changePart( "Necklace", "jewels", clothesDataBase.Necklace[jewelsIndex][necklaceIndex] )

    neckLace:setScale( bodyPartsPositionConfig.Necklace.fullScale, bodyPartsPositionConfig.Necklace.fullScale )
    neckLace:setPosition( bodyPartsPositionConfig.Necklace.x, bodyPartsPositionConfig.Necklace.y )
end

function BodyCharacter:updateBraceletByPose(bodyIndex, jewelsIndex)
    bodyIndex = bodyIndex or self:getBodyIndex()
    jewelsIndex = jewelsIndex or self:getJewelsIndex()
    local braceletPoses = self.handsPoses.right
    local braceletIndex = braceletPoses[bodyIndex]

    local bracelet = self:changePart( "Bracelet", "jewels", clothesDataBase.Bracelet[jewelsIndex][braceletIndex] )
    bracelet:setScale( bodyPartsPositionConfig.Bracelet.fullScale, bodyPartsPositionConfig.Bracelet.fullScale )
    bracelet:setPosition( bodyPartsPositionConfig.Bracelet.x, bodyPartsPositionConfig.Bracelet.y )
end

function BodyCharacter:updateBagByPose(bodyIndex)
    local bagIndex = self:getBagIndex()
    bodyIndex = bodyIndex or self:getBodyIndex()

    self:updateLeftHandBag(bodyIndex)
end

function BodyCharacter:updateRightHandBag(bodyIndex)
    local bag = self:getPart("Bag")

    if bodyIndex == 2 or bodyIndex == 3 then
        bag:show()
    else
        bag:hide()
    end
end

function BodyCharacter:updateLeftHandBag(bodyIndex)
    local bag = self:getPart("Bag")

    if bodyIndex == 1 or bodyIndex == 2 or bodyIndex == 6 then
        bag:show()
    else
        bag:hide()
    end
end

-------------------- **GETTERS** ----------------------------------
function BodyCharacter:getBagIndex()
    local bagSprite = self:getPart("Bag"):getSpriteName()

    return table.indexOf(clothesDataBase.Accessories.Bags, bagSprite)
end

-- Body index is also a pose index
function BodyCharacter:getBodyIndex()
    local bodySprite = self:getPart("Body"):getSpriteName()

    return table.indexOf(bodyDataBase.Bodies, bodySprite)
end

function BodyCharacter:getDownIndex()
    local downSprite = self:getPart("Down"):getSpriteName()

    return table.indexOf(clothesDataBase.Down, downSprite)
end

function BodyCharacter:getDressIndex()
    local dressTable = clothesDataBase.Dress
    local dressSprite = self:getPart("Dress"):getSpriteName()

    for i = 1, #dressTable do
        if dressTable[i].base == dressSprite then
            return i
        end
    end
end

function BodyCharacter:getGlassesIndex()
    local glassesSprite = self:getPart("Glasses"):getSpriteName()

    return table.indexOf(clothesDataBase.Accessories.Glasses, glassesSprite)
end

function  BodyCharacter:getJewelsIndex(bodyIndex)
    bodyIndex = bodyIndex or self:getBodyIndex()
    local earringsSprite = self:getPart("Earrings"):getSpriteName()

    return table.indexOf(clothesDataBase.Earrings, earringsSprite)
end

function BodyCharacter:getShoesIndex()
    local shoesSprite = self:getPart("Shoes"):getSpriteName()

    return table.indexOf(clothesDataBase.Shoes, shoesSprite)
end

function BodyCharacter:getSocksIndex()
    local socksSprite = self:getPart("Socks"):getSpriteName()

    return table.indexOf(clothesDataBase.Socks, socksSprite)
end

function BodyCharacter:getUpIndex()
    local upTable = clothesDataBase.Up
    local upSprite = self:getPart("Up"):getSpriteName()

    for i = 1, #upTable do
        if upTable[i].base == upSprite then
            return i
        end
    end
end

-- If you want to get idex of concrete wear pass upWearEnum in arguments
function BodyCharacter:getUpWearIndex(upWearEnum)
    local upWearType = self:getUpWearType()

    if      upWearType == Enums.UpWearTypes.Up    then return self:getUpIndex()
    elseif  upWearType == Enums.UpWearTypes.Dress then return self:getDressIndex()
    else error("upWearType has incorrect value")
    end
end

function BodyCharacter:getUpWearType()
    return self.metaData.upWearType
end

-------------------- **SETTERS** ----------------------------------

function BodyCharacter:setBody(bodyIndex)
    local upWearType = self:getUpWearType()

    local upWearTable = (upWearType == Enums.UpWearTypes.Up) and clothesDataBase.Up or clothesDataBase.Dress
    local upWearSpriteSheet = (upWearTable == clothesDataBase.Up) and "up{n}" or "dress{n}"

    if bodyIndex == nil then
        bodyIndex = self:getBodyIndex()
        bodyIndex = (bodyIndex < #bodyDataBase.Bodies) and bodyIndex + 1 or 1
    elseif bodyIndex <= 0 then
        bodyIndex = self:getBodyIndex()
        bodyIndex = (bodyIndex - 1 < 1) and #bodyDataBase.Bodies or bodyIndex - 1
    end

    local rightWristSprite = ""
    local leftWristSprite = ""

    if bodyIndex == 3 or bodyIndex == 4 or bodyIndex == 5 then leftWristSprite  = bodyDataBase.Wrists.left  end
    if bodyIndex == 1 or bodyIndex == 4 or bodyIndex == 8 then rightWristSprite = bodyDataBase.Wrists.right end

    self:changePart( "Right_Wrist", "body", rightWristSprite )
    self:changePart( "Left_Wrist", "body", leftWristSprite )

    self:changePart( "Body", "body", bodyDataBase.Bodies[bodyIndex] )

    self:updateByPose(bodyIndex)

    Managers.SpriteManager:cleanSpriteSheets()
end

function BodyCharacter:setGlasses(glassIndex)
    if glassIndex == NULL then self:removeRender("Glasses"); return end

    local glasses = self:changePart( "Glasses", "accessories",
        clothesDataBase.Accessories.Glasses[glassIndex] )

    glasses:setPosition(bodyPartsPositionConfig.Glasses.x, bodyPartsPositionConfig.Glasses.y)

    Managers.SpriteManager:cleanSpriteSheets()
end

function BodyCharacter:setBag(bagIndex)
    if bagIndex == NULL then self:removeRender("Bag"); return end

    self:setBody(1)

    local bag = self:changePart( "Bag", "accessories", clothesDataBase.Accessories.Bags[bagIndex] )

    bag:setPosition( bodyPartsPositionConfig.Bag.x, bodyPartsPositionConfig.Bag.y )
    self:updateBagByPose()

    Managers.SpriteManager:cleanSpriteSheets()
end

function BodyCharacter:setEarrings(jewelsIndex)
    if jewelsIndex == NULL then self:removeRender("Earrings"); return end

    local earrings = self:changePart( "Earrings","jewels", clothesDataBase.Earrings[jewelsIndex] )
    earrings:setScale( bodyPartsPositionConfig.Earrings.fullScale, bodyPartsPositionConfig.Earrings.fullScale )
    earrings:setPosition( bodyPartsPositionConfig.Earrings.x, bodyPartsPositionConfig.Earrings.y )
end

function BodyCharacter:setNecklace(jewelsIndex)
    if jewelsIndex == NULL then self:removeRender("Necklace"); return end

    self:updateNecklaceByPose(nil, jewelsIndex)
end

function BodyCharacter:setBracelet(jewelsIndex)
    if jewelsIndex == NULL then self:removeRender("Bracelet"); return end

    self:updateBraceletByPose(nil, jewelsIndex)
end

function BodyCharacter:setJewels(jewelsIndex)
    self:setEarrings(jewelsIndex)
    self:setNecklace(jewelsIndex)
    self:setBracelet(jewelsIndex)
end

function BodyCharacter:setUp(upIndex)
    if upIndex == nil or upIndex <= NULL or upIndex > #clothesDataBase.Up then upIndex = 1 end

    local bodyIndex = self:getBodyIndex()
    local upWearType = self:getUpWearType()

    if upWearType == Enums.UpWearTypes.Dress then
        self:removeRender("Dress_Composite")
        self:setDown(1)
    end

    local up = self:changePart( "Up", "up{n}", clothesDataBase.Up[upIndex].base )

    up:setPosition( bodyPartsPositionConfig.Up.x, bodyPartsPositionConfig.Up.y )

    self.metaData.upWearType = Enums.UpWearTypes.Up
    self:updateUpByPose( bodyIndex )

    Managers.SpriteManager:cleanSpriteSheets()
end

function BodyCharacter:setDown(downIndex)
    if downIndex == nil or downIndex <= NULL or downIndex > #clothesDataBase.Down then downIndex = 1 end

    local down = self:changePart( "Down", "down", clothesDataBase.Down[downIndex] )
    local upWearType = self:getUpWearType()

    if upWearType == Enums.UpWearTypes.Dress then
        self.metaData.upWearType = Enums.UpWearTypes.Up
        self:removeRender("Dress_Composite")
        self:setUp(1)
    end

    down:setPosition( bodyPartsPositionConfig.Down.x, bodyPartsPositionConfig.Down.y )
end

function BodyCharacter:setDress(dressIndex)
    if dressIndex == nil or dressIndex <= NULL or dressIndex > #clothesDataBase.Dress then
        self:setDown(1)
        self:setUp(1)
        return
    end

    local bodyIndex = self:getBodyIndex()
    local upWearType = self:getUpWearType()

    if upWearType == Enums.UpWearTypes.Up then
        self:removeRender("Up_Composite")
        self:removeRender("Down")
    end

    local dress = self:changePart( "Dress", "dress{n}", clothesDataBase.Dress[dressIndex].base )
    dress:setPosition( bodyPartsPositionConfig.Dress.x, bodyPartsPositionConfig.Dress.y )

    self.metaData.upWearType = Enums.UpWearTypes.Dress
    self:updateUpByPose( bodyIndex )

    Managers.SpriteManager:cleanSpriteSheets()
end

function BodyCharacter:setSocks(socksIndex)
    if socksIndex == NULL then self:removeRender("Socks"); return; end

    local socks = self:changePart( "Socks", "socks", clothesDataBase.Socks[socksIndex] )
    socks:setPosition( bodyPartsPositionConfig.Socks.x, bodyPartsPositionConfig.Socks.y )
end

function BodyCharacter:setShoes(shoesIndex)
    if shoesIndex == NULL then self:removeRender("Shoes"); return; end

    local shoes = self:changePart( "Shoes", "shoes", clothesDataBase.Shoes[shoesIndex] )
    shoes:setPosition( bodyPartsPositionConfig.Shoes.x, bodyPartsPositionConfig.Shoes.y )
end

return BodyCharacter