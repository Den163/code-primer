local CharacterFactory = {}

local Composite = require "src.CharacterComposite.Composite"
local SpriteComponent = require "src.CharacterComposite.SpriteComponent"
local ImageComponent = require "src.CharacterComposite.ImageComponent"
local bodyConfig = require "src.config.bodyPartsPositionConfig"

-- returns node of treeData by a nodeName
function CharacterFactory.GetNode(tree, nodeName)
    local searchedNode

    for name, node in pairs(tree.parts) do
        if name == nodeName then
            searchedNode = node
            break
        end

        if node.parts then
            searchedNode = CharacterFactory.GetNode(node, nodeName)
        end

        if searchedNode then break end
    end

    return searchedNode
end

local GetNode = CharacterFactory.GetNode

function CharacterFactory:deserialize(dataNode)
  local characterObject = Composite:deserialize(dataNode)

  return characterObject
end

function CharacterFactory:makeNode(dataTree, nodeName)
  local this = self

  local dataNode = GetNode(dataTree, nodeName)
  local face = this:deserialize(dataNode)

  return face
end

function CharacterFactory:makePortrait()
  local this = self
  local dataTree = Managers.DataManager:getPortraitTreeData().Portrait_Tree
  local hairLengthIndex
  local hairComponent
  local hairX
  local hairY

  local backHairData = GetNode(dataTree, "Hairs_Back_Composite")
  local faceData = GetNode(dataTree, "Face_Composite")
  local frontHairData = GetNode(dataTree, "Hairs_Front_Composite")
  local portrait = Composite:new("Portrait_Tree")
  local backHairComposite = portrait:addPart("Hairs_Back_Composite", this:deserialize(backHairData))

  portrait:addPart("Face_Composite", this:deserialize(faceData))

  local frontHairComposite = portrait:addPart("Hairs_Front_Composite", this:deserialize(frontHairData))

  hairComponent = GetNode(dataTree, "Hairs_Back_First_Layer")

  if hairComponent.spriteSheet then
    if string.find(hairComponent.spriteSheet, "short") then
      hairLengthIndex =  1
    elseif string.find(hairComponent.spriteSheet, "medium") then
      hairLengthIndex = 2
    elseif string.find(hairComponent.spriteSheet, "long") then
      hairLengthIndex = 3
    end
  else
    hairLengthIndex = 1
  end

  hairX = bodyConfig.PortraitHairs.x[hairLengthIndex]
  hairY = bodyConfig.PortraitHairs.y[hairLengthIndex]

  backHairComposite:setPosition(hairX, hairY)
  frontHairComposite:setPosition(hairX, hairY)

  backHairComposite:setScale(1.24, 1.24)
  frontHairComposite:setScale(1.24, 1.24)

  portrait:getPart("Nose"):setPosition(bodyConfig.Nose.x, bodyConfig.Nose.y)
  portrait:getPart("Brows_Composite"):setPosition(bodyConfig.Brows.x, bodyConfig.Brows.y)
  portrait:getPart("Lips_Composite"):setPosition(bodyConfig.Lips.x, bodyConfig.Lips.y)
  portrait:getPart("Eye_Composite"):setPosition(bodyConfig.Eyes.x, bodyConfig.Eyes.y)

  local face = portrait:getPart("Face_Composite")
  face:setScale(1.3, 1.3)
  face:setPosition(2, -8)

  portrait:setPosition(0, 10)

  return portrait
end

function CharacterFactory:makeStartCharacter()
  local this = self

  local portrait = this:makePortrait()
  local body = SpriteComponent:new("startScene", "doll_body_start")
  portrait:addPart("Body", body)

  body:setPosition(8, 170)

  portrait:setZ("Hairs_Back_Composite", 1)
  portrait:setZ("Body", 2)
  portrait:setZ("Face_Composite", 3)
  portrait:setZ("Hairs_Front_Composite", 4)

  return portrait
end

local setFaceTransform

function CharacterFactory:makeFull()
  local this = self
  local dataTree = Managers.DataManager:getBodyTreeData().Body_Tree

  local BODY_SCALE_MODIFIER = 1.3

  local bodyComposite = this:deserialize(dataTree)
  local CharacterComposite = Composite:new("Character_Tree")

  local Hairs_Back, Face, Hairs_Front = self:makePreRenderedFace()

  bodyComposite:addPart( "Hairs_Back", Hairs_Back )

  bodyComposite:addPart( "Face", Face )
  bodyComposite:addPart( "Hairs_Front", Hairs_Front )

  CharacterComposite:addPart( "Body_Tree", bodyComposite )

  setFaceTransform( Face, Hairs_Back, Hairs_Front  )

  bodyComposite:setScale( BODY_SCALE_MODIFIER, BODY_SCALE_MODIFIER )

  return CharacterComposite
end

function CharacterFactory:makeFullPreRendered()
  local this = self
  local BODY_SCALE_MODIFIER = 0.77 / (display.pixelWidth / display.actualContentWidth)
  local ACCESSORIES_SCALE_MODIFIER = 1 / (display.pixelWidth / display.actualContentWidth)
  local CHARACTER_SCALE_MODIFIER = 1.13

  local CharacterComposite = Composite:new("Character_Tree")
  local Hairs_Back, Face, Hairs_Front = self:makePreRenderedFace()
  local Body = ImageComponent:new( "Body_Tree.png", system.DocumentsDirectory )
  local Glasses = ImageComponent:new( "Glasses.png", system.DocumentsDirectory )
  local Earrings = ImageComponent:new( "Earrings.png", system.DocumentsDirectory )

  CharacterComposite:addPart( "Hairs_Back", Hairs_Back )
  CharacterComposite:addPart( "Body", Body )
  CharacterComposite:addPart( "Face", Face )
  CharacterComposite:addPart( "Earrings", Earrings )
  CharacterComposite:addPart( "Hairs_Front", Hairs_Front )
  CharacterComposite:addPart( "Glasses", Glasses )

  Body:setScale( BODY_SCALE_MODIFIER, BODY_SCALE_MODIFIER )
  Earrings:setScale( ACCESSORIES_SCALE_MODIFIER, ACCESSORIES_SCALE_MODIFIER )
  Glasses:setScale( ACCESSORIES_SCALE_MODIFIER, ACCESSORIES_SCALE_MODIFIER )

  setFaceTransform( Face, Hairs_Back, Hairs_Front )

  CharacterComposite:setScale( CHARACTER_SCALE_MODIFIER, CHARACTER_SCALE_MODIFIER )

  return CharacterComposite
end

function CharacterFactory:makePreRenderedFace()
  local Hairs_Back = ImageComponent:new("Hairs_Back_Composite.png", system.DocumentsDirectory)
  local Face = ImageComponent:new("Face_Composite.png", system.DocumentsDirectory)
  local Hairs_Front = ImageComponent:new("Hairs_Front_Composite.png", system.DocumentsDirectory)

  return Hairs_Back, Face, Hairs_Front
end

function CharacterFactory:extend()
  local extendObject = {}

  self.__index = self
  setmetatable(extendObject, self)

  return extendObject
end

function setFaceTransform( face, hairs_back, hairs_front )
  local hairLengthIndex = Managers.DataManager:getHairsLength()
  local FACE_SCALE_MODIFIER = 0.79 / (display.pixelWidth / display.actualContentWidth)
  local FACE_X = bodyConfig.FullFace.x
  local FACE_Y = bodyConfig.FullFace.y
  local HAIRS_X = FACE_X + bodyConfig.FullHairs.x[hairLengthIndex]
  local HAIRS_Y = FACE_Y + bodyConfig.FullHairs.y[hairLengthIndex]

  hairs_back:setScale( FACE_SCALE_MODIFIER, FACE_SCALE_MODIFIER )
  face:setScale( FACE_SCALE_MODIFIER, FACE_SCALE_MODIFIER )
  hairs_front:setScale( FACE_SCALE_MODIFIER, FACE_SCALE_MODIFIER )

  hairs_back:setPosition( HAIRS_X, HAIRS_Y  )
  face:setPosition( FACE_X, FACE_Y )
  hairs_front:setPosition( HAIRS_X, HAIRS_Y )
end

return CharacterFactory