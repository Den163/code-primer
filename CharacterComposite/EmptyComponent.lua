local BaseComponent = require "src.CharacterComposite.BaseComponent"
local EmptyComponent = BaseComponent:extend()

function EmptyComponent:new()
    local newObject = {}

    self.__index = self
    setmetatable(newObject, self)

    newObject.group = display.newGroup()
    -- The empty component contains minimum graphics to support all functionality for component
    newObject.view = display.newCircle( 0, 0, 1)

    newObject.view:setFillColor( 0, 0, 0, 0 )
    newObject.group:insert(newObject.view)

    newObject.metaData = {}
    newObject.z = 1

    return newObject
end

function EmptyComponent:getSpriteName()
    return nil
end

function EmptyComponent:getColor()
    return self.metaData.color
end

function EmptyComponent:getExposure()
    return self.metaData.exposure
end

function EmptyComponent:getPosition()
    return {
        x = self.metaData.x,
        y = self.metaData.y
    }
end

function EmptyComponent:getScale()
    return {
        xScale = self.metaData.xScale,
        yScale = self.metaData.yScale
    }
end

function EmptyComponent:getZ()
    return self.metaData.z
end

function EmptyComponent:setPosition(x, y)
    self.metaData.x = x
    self.metaData.y = y
end

function EmptyComponent:setScale(xScale, yScale)
    self.metaData.xScale = xScale
    self.metaData.yScale = yScale
end

function EmptyComponent:setZ(z)
    self.metaData.z = z
end

function EmptyComponent:setGroup(group)
    group:insert(self.group)
end

function EmptyComponent:setColor(color, exposure)
    self.metaData.color = color
    self.metaData.exposure = exposure
end

function EmptyComponent:hide()
end

function EmptyComponent:show()
end

function EmptyComponent:serialize(characterDataTree)
    local this = self
    local metaData = this.metaData
    characterDataTree[metaData.name] = {}

    local treeNode = characterDataTree[metaData.name]

    treeNode.name = metaData.name
    treeNode.x = metaData.x
    treeNode.y = metaData.y
    treeNode.z = metaData.z
    treeNode.xScale = metaData.xScale
    treeNode.yScale = metaData.yScale

    return characterDataTree
end

function EmptyComponent:deserialize(dataNode)
    local component = EmptyComponent:new()
    local metaData = component.metaData

    metaData.x = dataNode.x
    metaData.y = dataNode.y
    metaData.z = dataNode.z
    metaData.xScale = dataNode.xScale
    metaData.yScale = dataNode.yScale

    return component
end

function EmptyComponent:destroy()
end

return EmptyComponent